#!/bin/bash
# Created: 20130528
# Author: Kamran Azeem
# Summary: Script to send MySQL Tuner report to DevTeam.
# Note: This script contains mysql root password. It should not have permissions other than 0700.

## mysqltuner --user=root --pass=mysql-password

# START - User Configurable Variables
#
DBUSER=root
DBPASSWORD=mysql-password
MAILINGLIST="user@example.com  user2@example.net"
TUNER=$(which mysqltuner)
#

# Please create a .my.cnf file in the user's home directory, and set it's permission to 0600
# [client]
# user=root
# password=mysql-password

# END - User Configurable Variables


# START - Application Logic

HOSTNAME=$(hostname -s)
TODAY=$(date +"%Y-%m-%d")

if [ ! -x ${TUNER} ] ; then
  echo "${TUNER} could not be found. Please install mysql tuner."
  exit 9
fi

## ${TUNER} --user="${DBUSER}" --pass="${DBPASSWORD}" > /tmp/MySQL-Tuner-Report-${HOSTNAME}-${TODAY}.txt
${TUNER} --nocolor > /tmp/MySQL-Tuner-Report-${HOSTNAME}-${TODAY}.txt

cat /tmp/MySQL-Tuner-Report-${HOSTNAME}-${TODAY}.txt | mail -s "MySQL Tuner Report for ${TODAY}, for DB service  running  on ${HOSTNAME}" ${MAILINGLIST}

# END - Application Logic


