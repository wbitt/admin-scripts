# Admin scripts
Consolidating various admin scripts I have written over the years in this repository.

* BackupDatabases.sh
* BackupWebSites.sh
* checkload.sh
* Copy-Slow-Queries-Log.sh
* MySQLTunerReport.sh
* Print-Total-RSS-RES-Memory.sh
* sensors-log.sh
* SwapCheck.sh
