#!/bin/sh
########################################################################
# Author:       Muhammad Kamran Azeem (kamran@wbitt.com)
# Created:      2008-04-24
# Modified:     20130803, 20120120, 20080720, 20080628
# Note:         For FTP, make sure that the file ~/.netrc exists in the home directory
#               of the user running this script. The permissions must be 0600.
#               The syntax of .netrc file is:
#               machine ftpserver.somedomain.com
#               login ftpusername
#               password somepassword
#
# For MySQL login without root password, you can also create a .my.cnf file in the user's home directory.
# Normally in the /root/ as most of the time it is root executing cron jobs.
# .my.cnf:
# [mysql]
# user=user
# password=password
#
# Note2:        This script must have permissions not more than 0700, and owned by root . Because
#               this script contains username and password of your mysql admin user.
#
# Note 3:       You will need to frequently delete the old backups both from local disk,
#               as well as from the FTP server.
#
# Note 4:       Setup a cronjob as:-
#               0 3 * * * /backups/dbbackup.sh
#
########################################################################

# Notes - Variables passed from outside the script (from environment): 
# * MYSQL_* variables are passed from outside. 
#     Without MYSQL_* variables this script will not work.
# * Need to have a location of backup directory, which can NEVER be (/).


########## START - User configuration ##################################

# DO NOT BACKUP these databases. 
# The list is db names separated by space, enclosed by quotation marks.
IGNORE_DBS="mysql information_schema performance_schema test"

# This is the time period for which the backups will be retained.
# Backups before this date will be deleted! Make selection carefully.
# The value is number of days before $TODAY.
# Note: This works only if the BACKUP_DESTINATION_DIRECTORY is a persistent storage.
#       If it is not, the script will simply not find old backup,
#          and will simply not delete it.
#
RETENTION_PERIOD=14

########## END - User configuration ####################################



####### Sanaty checking - START ##########

if [ -z "${REMOVE_LAST_BACKUP_DATE}" ] || [ -z "${BACKUP_DIR}" ] || [ "${BACKUP_DIR}" == "/" ] ; then

  echo "REMOVE_LAST_BACKUP_DATE , and BACKUP_DIR cannot be empty, nor it can be equal to / ."
  echo "If you do so, you can potentially delete all data on your disks."
  echo "Fix this, and re-run the program."
  exit 1

fi


if [[ -z "${MYSQL_HOST}" ]] || [[ -z "${MYSQL_ROOT_USER}" ]] || [[ -z "${MYSQL_ROOT_PASSWORD}" ]] || [[ -z "${BACKUP_DESTINATION_DIRECTORY}" ]] ; then

  echo "One of the required variables are empty . Cannot proceed with backup ..."
  exit 1

fi 

if [[ "${BACKUP_DESTINATION_DIRECTORY}" == "/" ]] ; then
  echo "BACKUP_DESTINATION_DIRECTORY was found as '/', which is not allowed."
  echo "Set value of this variable to a 'non-system' directory - e.g. '/backups'"
  echo "The directory needs to exist on the file-system."
  echo "In case of a Docker container, it can be a (temporary/persistent) volume."
  exit 1
fi

####### Sanaty checking - END ##########

MYSQLHOST="${MYSQL_HOST}"          # MySQL Hostname or the name of the service to connect to
MYSQLUSER="${MYSQL_ROOT_USER}"     # MySQL root USERNAME
MYSQLPASS="${MYSQL_ROOT_PASSWORD}" # MySQL root PASSWORD

# Backup Dest directory, change this if you have some other location
BACKUP_DIR="${BACKUP_DESTINATION_DIRECTORY}"


# Get hostname
HOSTNAME=$(hostname)

## TODAY=$(date +%Y%m%d)
TODAY=$(date +%Y%m%d)

# Main directory where today's backups will be stored
TODAY_BACKUP_DIR=${TODAY}

# Directory to remove after backup is complete.
REMOVE_LAST_BACKUP_DATE=$(date --date="${RETENTION_PERIOD} days ago" +%Y%m%d)

# Password file:
MYSQL_DEFAULTS_FILE=${HOME}/${TODAY}.my.cnf

# Paths to various utlities / programs ...
TAR=$(which tar)
MYSQL=$(which mysql)
MYSQLDUMP=$(which mysqldump)
GZIP=$(which gzip)
BZIP2=$(which bzip2)
RSYNC=$(which rsync)
NICE=$(which nice)


# File to store current backup file
FILE=""

# Store list of databases
DB_LIST=""


echo "Here is the configuration, you specified:-"
echo ""
echo "MYSQLUSER=${MYSQLUSER}"
echo "MYSQLPASS=[Edit this script file to see the password]!"
echo "MYSQLHOST=${MYSQLHOST}"
echo "BACKUP_DIR=${BACKUP_DIR}"
echo "TODAY_BACKUP_DIR=${TODAY_BACKUP_DIR}"
echo "RETENTION_PERIOD=${RETENTION_PERIOD}"
echo "REMOVE_LAST_BACKUP_DATE=${REMOVE_LAST_BACKUP_DATE}"
echo "System HOSTNAME: ${HOSTNAME}"
echo "IGNORE_DBS=${IGNORE_DBS}"
echo "SYNCTOSECONDDISK=${SYNCTOSECONDDISK}"
echo "SECONDDISKDIR=${SECONDDISKDIR}"
echo "SYNCTOREMOTESERVER=${SYNCTOREMOTESERVER}"
echo "REMOTESERVER=${REMOTESERVER}"
echo "REMOTEUSER=${REMOTEUSER}"
echo "REMOTEDIR=${REMOTEDIR}"


# Initiating main program


if [ ! -d ${BACKUP_DIR}/${TODAY_BACKUP_DIR} ]; then
  mkdir -p ${BACKUP_DIR}/${TODAY_BACKUP_DIR}
else
  echo ""
  echo "Oops! Old backup found!"
  echo "Perhaps the backup for this date (${TODAY}) has already been performed."
  echo "If this is not what you wanted,"
  echo "  you need to manually remove the directory ${BACKUP_DIR}/${TODAY_BACKUP_DIR}"
  echo ""
  exit 1
fi


############ START - Create .my.cnf #################


cat > ${MYSQL_DEFAULTS_FILE} <<EOF
[mysqldump]
host=${MYSQLHOST}
user=${MYSQLUSER}
password=${MYSQLPASS}
EOF

chmod 0600 ${MYSQL_DEFAULTS_FILE}

############ END - Create .my.cnf #################



echo "Starting DB backup/dump...."


# Get all database list first
DB_LIST=$($MYSQL -u $MYSQLUSER -h $MYSQLHOST -p$MYSQLPASS -Bse 'show databases')

for DB in ${DB_LIST}; do

  skipdb="F"

  if [[ "${IGNORE_DBS}" != "" ]] && [[ "${IGNORE_DBS}" =~ "${DB}" ]] ; then
    echo "***** This DB (${DB}) is set to be ignored in IGNORE_DBS variable. Skipping ..."
    skipdb="T"
  fi

  if [ "${skipdb}" == "F" ] ; then
    FILE="${BACKUP_DIR}/${TODAY_BACKUP_DIR}/${DB}.${HOSTNAME}.${TODAY}.dump"
    echo "--------------------------------------------------------------------"
    echo "Performing dump/backup of database - ${DB} ... as ${FILE} ..."
    echo "Executing: ${NICE} -n 19 ${MYSQLDUMP}  --defaults-file=${MYSQL_DEFAULTS_FILE} --routines --triggers ${DB} > ${FILE}"
    ${NICE} -n 19 ${MYSQLDUMP} --defaults-file=${MYSQL_DEFAULTS_FILE} \
      --routines --triggers ${DB} > ${FILE}
  fi
done

echo ""
echo ""
echo "Database Dump/Backup complete."
echo ""
echo "=================================================================="
echo ""
echo ""


echo "Time to remove old backups from local system  and remote FTP server."
echo "First remove the directory $BACKUP_DIR/$REMOVE_LAST_BACKUP_DATE from local system..."
echo "NOTE: Make sure you don't have any typing mistake in the next lines below,"
echo "as IT HAS POTENTIAL TO WIPE OFF YOUR SERVER HARD DISK CLEAN !!!"
echo "YOU HAVE BEEN WARNED."


if [ -z "${BACKUP_DIR}" ] || [ -z "${REMOVE_LAST_BACKUP_DATE}" ] ; then
  echo "Cannot remove old backup ${BACKUP_DIR}/${REMOVE_LAST_BACKUP_DATE}."
  echo "This seems to be your system root!"
else
  echo "Executing: rm -fr ${BACKUP_DIR}/${REMOVE_LAST_BACKUP_DATE}*"
  rm -fr ${BACKUP_DIR}/${REMOVE_LAST_BACKUP_DATE}* && echo "Old backup directory successfully removed."
fi 



if [ "${SYNCTOREMOTESERVER}" == "Y" ] || [ "${SYNCTOREMOTESERVER}" == "y" ]; then

  echo "Starting sync of databases to remote server ${REMOTESERVER}"
  # It is important to use nice (+ve)19 , so our backup process is not a burden
  # on the running system's resources.
  echo "Executing: ${NICE} -n 19 ${RSYNC} -av   ${BACKUP_DIR}/${TODAY_BACKUP_DIR} ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/"
  ${NICE} -n 19 ${RSYNC} -av   ${BACKUP_DIR}/${TODAY_BACKUP_DIR}  ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/
  echo "Sync of local databases to remote server completed."

fi

exit 0


