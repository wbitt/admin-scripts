#!/bin/bash 
###########################################################################################
# Filename:	sitesbackup.sh
# Purpose:	Backup web content hosted on this server.
# Author:       Muhammad Kamran Azeem (kamran@wbitt.com)
# Created:      20080628
# Modified:     20120123, 20080720, 20080628
# Note:         For FTP, make sure that the file ~/.netrc exists in the home directory
#               of the user running this script. The permissions must be 0600.
#               The syntax of .netrc file is:
#               machine ftpserver.somedomain.com
#               login ftpusername
#               password somepassword
#
# Note2:        This script must have permissions not more than 0700, and owned by root . Because
#               this script contains username and password of your mysql admin user,
# 		and the FTP admin user.
#
# Note 3:       You will need to frequently delete the old backups both from local disk,
#               as well as from the FTP server.
#
# Note 4:       Setup a cronjob as:-
#               0 2 * * * /backups/sitesbackup.sh
#
############################################################################################
#


####### User configuration - START #######################


# Backup Dest directory, change this if you have someother location.
# Necessary directories will be created under this directory.
BASEBACKUPDIR="/data/backups/sites"

# Where is the web content?
WEBCONTENTDIR="/data/vhosts"

# DO NOT BACKUP these sites / directories. These will be names of actual 
# directory and file names you wish to skip, separated by space.
IGNORESITES="test.com example.com"

# What will be the oldest backup retained? Specify it here. 
# The value is in number of days. If you specify 14 and run this backbackup 
# every day, then 14 copies will be stored. however if you specify 14
# and run the backup every 3 days, then only 4 copies will be saved.
RETENTIONPERIOD=14

# Options:
	
# Backup to FTP location?
SYNCTOFTP="N"
FTPSERVER="ftpbackup.example.com"
FTPUSER=""
FTPPASSWORD=""
# You can also use ~/.netrc file if you dont want to put usernames and password here.
# Just make sure that the ~.netrc file exists in the home directory of the user
# which runs this script. In our case, it is root.

# Perform Sync of vhosts to another localtion on another disk on the same server? 
# e.g. Second Hard drive? Need to specify the TARGETDIR
SYNCTOSECONDDISK="N"
SECONDDISKDIR="/data/backups/sites/"

# Perform Sync to another Server? (Need to specify REMOTESERVER, REMOTEUSER and REMOTEDIR)
# Also, make sure that the ssh public key of user root on this server is added to authorized_keys file of the remote user.

SYNCTOREMOTESERVER="N"
REMOTESERVER="db.example.com"
REMOTEUSER="backup"
REMOTEDIR="/backups/www.example.com/vhosts"

########## User configuration - END #######################


TODAY=$(date +%Y%m%d)
REMOVELASTBACKUPDATE=$(date --date="$RETENTIONPERIOD days ago" +%Y%m%d)

# Paths to various utlities / programs ...
TAR=$(which tar)
RSYNC=$(which rsync)
GZIP=$(which gzip)
BZIP2=$(which bzip2)
NICE=$(which nice)

# lftp is the FTP client program used in this script, for severeal reasons
# If you don't have that installed. You better install it before continuing.
FTPCLIENT="lftp"

# Main directory where today's backups will be stored
TODAYBACKUPDIR=${TODAY}

# Get hostname
HOSTNAME=$(hostname)
## HOSTNAME="www.example.com"
BACKUPDIR="${BASEBACKUPDIR}/${HOSTNAME}"

echo "Here is the configuration, you specified:-"
echo ""
echo "WEBCONTENTDIR=${WEBCONTENTDIR}"
echo "BASEBACKUPDIR=${BASEBACKUPDIR}"
echo "RETENTIONPERIOD=${RETENTIONPERIOD}"
echo "IGNORESITES=${IGNORESITES}"
echo "SYNCTOFTP=${SYNCTOFTP}"
echo "FTPSERVER=${FTPSERVER}"
echo "FTPUSER=${FTPUSER}"
echo "FTPPASSWORD=${FTPPASSWORD}"
echo "SYNCTOSECONDDISK=${SYNCTOSECONDDISK}"
echo "SECONDDISKDIR=${SECONDDISKDIR}"
echo "SYNCTOREMOTESERVER=${SYNCTOREMOTESERVER}"
echo "REMOTESERVER=${REMOTESERVER}"
echo "REMOTEUSER=${REMOTEUSER}"
echo "REMOTEDIR=${REMOTEDIR}"
echo ""
echo "BACKUPDIR=${BACKUPDIR}"
echo "TODAYBACKUPDIR=${TODAYBACKUPDIR}"

echo "REMOVELASTBACKUPDATE=${REMOVELASTBACKUPDATE}"
echo "System Hostname: ${HOSTNAME}"
echo ""

####### Sanaty checking - START ##########
if [ -z "${REMOVELASTBACKUPDATE}" ] || [ "${REMOVELASTBACKUPDATE}" == "/" ] || [ -z "$BACKUPDIR" ] || [ "$BACKUPDIR" == "/" ] ; then
  echo "REMOVELASTBACKUPDATE , and BACKUPDIR cannot be empty, nor it can be equal to / ."
  echo "If you do so, you can potentially  WIPE THE DISK CLEAN."
  echo "Fix this, and re-run the program."
  exit 9
fi

if [ "${SYNCTOFTP}" == "Y" ] && [ -z "$(which ${FTPCLIENT})" ] ; then
  echo "Please install lftp."
  exit 9
fi

####### Sanaty checking - END ##########



############### Define functions - START ################

############### Define functions - END ################


if [ ! -d ${BACKUPDIR}/${TODAYBACKUPDIR} ]; then
  mkdir -p ${BACKUPDIR}/${TODAYBACKUPDIR}
else
  echo ""
  echo "Oops! Old backup found! Perhaps the backup for this date (${TODAY}) has already been performed."
  echo "If this is not what you wanted, you need to manually remove the directory ${BACKUPDIR}/${TODAYBACKUPDIR}"
  echo ""
  exit 1
fi


echo "Starting web-content backup of the sites inside ${WEBCONTENTDIR} ..."

# Store list of sites
# SITESLIST=""

# Get all websites list first
SITESLIST=$(ls ${WEBCONTENTDIR})

for SITE in ${SITESLIST} ; do
  skipsite="F"
  if [ "${IGNORESITES}" != "" ]; then
    for i in ${IGNORESITES}; do
      if [ "${SITE}" == "${i}" ]; then
	echo "***** SKIPPING SITE:  ${i}"
        skipsite="T"
      fi
    done
  fi

  if [ "${skipsite}" == "F" ] ; then
    FILE="${BACKUPDIR}/${TODAYBACKUPDIR}/${SITE}-${TODAY}.tar.bz2"
    echo "--------------------------------------------------------------------"
    echo "Backing up site ${SITE} ..."
    echo "Creating ${FILE} ..."
    echo "Executing: ${NICE} -n 19 ${TAR} cjf ${FILE} ${WEBCONTENTDIR}/${SITE} --exclude ${WEBCONTENTDIR}/${SITE}/logs/* "
    ${NICE} -n 19 ${TAR} cjf ${FILE} ${WEBCONTENTDIR}/${SITE} --exclude "${WEBCONTENTDIR}/${SITE}/logs/* "
  fi
done

echo ""
echo "Web-content backup of completed in ${BACKUPDIR}/${TODAYBACKUPDIR}"

if [ "${SYNCTOFTP}" == "Y" ] || [ "${SYNCTOFTP}" == "y" ] ; then
  echo "Starting FTP upload..."
  # Save current location in a variable. Will need it later.
  PWD=$(pwd)

  # Change to the backup directory, where the tar files are present.
  cd $BACKUPDIR/$TODAYBACKUPDIR

  # Create a directory on ftp server with today's date, and put the tar files over there.
##  $FTPCLIENT $FTPSERVER << EOF
##  mkdir $TODAY
##  cd $TODAY
##  mput *.tar.bz2
##  cd ..
##  rm -fr  $REMOVELASTBACKUPDATE
##  EOF

  echo "FTP Upload complete, and ${REMOVELASTBACKUPDATE} directory removed from FTP."

  # Now change back to where we were before FTP upload began
  cd ${PWD}
fi

echo "Time to remove old backups from local system."
echo "Removing the directory $BACKUPDIR/$REMOVELASTBACKUPDATE from local system..."
# NOTE: Make sure you dont have any typing mistake in the next lines below,
# as IT HAS POTENTIAL TO WIPE OFF YOUR SERVER HARD DISK CLEAN !!!
# YOU HAVE BEEN WARNED.

if [ -z "${BACKUPDIR}" ] || [ -z "${REMOVELASTBACKUPDATE}" ] ; then
  echo "Cannot remove old backup ${BACKUPDIR}/${REMOVELASTBACKUPDATE}. It looks like root / !!!"
else
  echo "Executing: rm -fr ${BACKUPDIR}/${REMOVELASTBACKUPDATE}"
  rm -fr ${BACKUPDIR}/${REMOVELASTBACKUPDATE}
fi


if [ "${SYNCTOSECONDDISK}" == "Y" ] ||  [ "${SYNCTOSECONDDISK}" == "y" ] ; then

  echo "Starting sync of vhosts, to the second local disk ${SECONDDISKDIR}/"
  # It is important to use nice -19 , so our backup process is not a burden 
  # on the running system's resources
  # Also, syncing logs is useless. We are concerned about the actual web-content.
  echo "Executing: ${NICE} -n 19 ${RSYNC} -av --exclude=\"logs/*\"  ${WEBCONTENTDIR}/   ${SECONDDISKDIR}/"
  ${NICE} -n 19 ${RSYNC} -av --exclude="logs/*"  ${WEBCONTENTDIR}/   ${SECONDDISKDIR}/ 
  echo "Sync  of local vhosts to the second local disk completed."
  echo ""
fi

if [ "${SYNCTOREMOTESERVER}" == "Y" ] || [ "${SYNCTOREMOTESERVER}" == "y" ]; then 

  echo "Starting sync of vhosts, to remote server ${REMOTESERVER}"
  # It is important to use nice (+ve)19 , so our backup process is not a burden 
  # on the running system's resources.
  # Also, syncing logs is useless. We are concerned about the actual web content, not logs.
  echo "Executing: ${NICE} -n 19 ${RSYNC} -av --exclude=\"logs/*\"  ${WEBCONTENTDIR}/ ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/"
  ${NICE} -n 19 ${RSYNC} -av --exclude="logs/*"  ${WEBCONTENTDIR}/  ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/
  echo "Sync of local vhosts to remote server competed."

fi

exit 0

