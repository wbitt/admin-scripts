#!/bin/bash
# Created: 20130528
# Author: Kamran Azeem

# START - User configurable variables
#
MYSQLLOGDIR=/var/log/mysql/
SLOWLOGFILE=slow-queries.log
TARGETDIR=/var/www/vhosts/finedistribution.com/logs/
GZIP=$(which gzip)
#
# END - User configurable variables


# START - Application Logic
echo "SLOWLOGFILE (source file): ${MYSQLLOGDIR}/${SLOWLOGFILE}"
echo "TARGETDIR (target directory): ${TARGETDIR}"
echo

if [ ! -r ${MYSQLLOGDIR}/${SLOWLOGFILE} ] ; then 
  echo "${MYSQLLOGDIR}/${SLOWLOGFILE} does not exist, or has permission issues. Please check."
  exit 9
fi

if [ ! -d ${TARGETDIR} ] ; then 
  echo "${TARGETDIR} does not exist, or has permission issues. Please check."
  exit 9
fi

echo "Copying file ${MYSQLLOGDIR}/${SLOWLOGFILE}  to ${TARGETDIR}."

cp -v ${MYSQLLOGDIR}/${SLOWLOGFILE} ${TARGETDIR}/ 
if [ -r ${TARGETDIR}/${SLOWLOGFILE}.gz ] ; then 
  rm -f ${TARGETDIR}/${SLOWLOGFILE}.gz 
fi

echo

echo "Compressing file ${TARGETDIR}/${SLOWLOGFILE}. Please wait..."
${GZIP} -v -9  ${TARGETDIR}/${SLOWLOGFILE} && echo "${TARGETDIR}/${SLOWLOGFILE}.gz created." || echo "File creation failed. Please check."

chmod +r ${TARGETDIR}/${SLOWLOGFILE}.gz





# END - Application Logic

