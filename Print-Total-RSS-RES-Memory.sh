#!/bin/bash
## ps v -C httpd --no-heading | awk '{sum+=$8} END {print "Sum: " sum}'

if [ "$1" == "" ] ; then
  echo "Please provide the name of the process to find the RSS memory consumption."
  echo "This script uses: ps v -C httpd --no-heading | awk '{sum+=$8} END {print \"Sum: \" sum}'"
  exit 9
fi

# The $8 in the command below is internal to awk.
/bin/ps v -C $1 --no-heading | awk '{sum+=$8} END {print sum}'

