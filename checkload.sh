#!/bin/bash
# Author: Muhammad Kamran Azeem
# Created: May 2009
# Last revised: 2011-05-14, 2010-01-24
# NOTE: This script may contain the password to the MySQL root.
#       Thus the permissions of this script should be 0700 and under ownership of root, only.
# To be more secure, it is best to use ~/.my.cnf and provide mysql password there for the mysql client, 
# and not provide mysql admin password in this script.

#### START - System Variables
#
# Note: Do not edit the variables in this section
HOSTNAME=$(hostname)
DATETIME=$(date "+%F-%T")
#
#### END - System Variables


###### Start - User configurable variables
#
# If you want to change the hostname manually, then this it the place to do that. 
# If not, then the program will take the system hostname.
## HOSTNAME="server.example.com"

# MAXLOAD is the threshhold. When load is higher, alert email is sent.
# If your system remains high on load avg, during normal operation, 
# and you consider it normal, such as 2, you can adjust the MAXLOAD variable to 3,
# to avoid being bombarded with high load emails.
MAXLOAD=2

# Email addresses in the MAILINGLIST variable should be separated by space. NOT comma.
MAILINGLIST="admin@example.com"

# NOTE: This script may contain the password to the MySQL root.
#       Thus the permissions of this script should be 0700, and owned by root only.
# You can configure mysql to allow mysql root connection from the os root, by using /root/.my.cnf file

###################################
## example]# cat /root/.my.cnf 
## [client]
## user=root
## password=password
## 
## # The following is for mysql command line utility.
## [mysql]
## user=root
## password=SecretPassword

## [mysqladmin]
## user=root
## password=SecretPassword

## [mysqldump]
## user=root
## password=SecretPassword

## [mysqldiff]
## user=root
## password=pass
###################################

MYSQLADMINUSER="root"
MYSQLADMINPASSWORD=""

# REPORTFILE gets overwritten each time the program runs.
REPORTFILE="/var/log/loadreport-${DATETIME}.log"

# LOGFILE gets the output of uptime only. Good for checking when the load was high.
LOGFILE="/var/log/serverload.log"


# To see the output of this program (the report file) on console/terminal, 
# set the value of the DEBUG variable to digit 1. (True). 
# Set to digit 0 (False) when you don't want to.
DEBUG=0

###### End - User configurable variables


####################### Start - Define Functions

separator() 
{
echo "============================================================================================" >> ${REPORTFILE}
echo "" >> ${REPORTFILE}
return
}

####################### End - Define Functions


### Add a log entry to the logfile, in addition to whatever happens further below in this script.
uptime >> ${LOGFILE}


# Load average: 1 min, 5 min, 15 min
ONEMINUTELOAD=$(uptime | sed -e "s/.*load average: \(.*\...\), .*\..., .*\.../\1/" -e "s/ //g")
if [ "$1" != "" ]; then
  ONEMINUTELOAD=$1
fi

FIVEMINUTELOAD=$(uptime | sed -e "s/.*load average: .*\..., \(.*\...\), .*\.../\1/" -e "s/ //g")
if [ "$1" != "" ]; then
  FIVEMINUTELOAD=$1
fi

INTEGERLOAD=$(echo ${FIVEMINUTELOAD} | cut -d. -f 1)

if [ ${INTEGERLOAD} -lt ${MAXLOAD} ]; then
  ## No need to print these values, if there is nothing to report. 
  ## Because this takes space in postmaster email box, throug cron.
  ##  echo "5 minute load average is: ${FIVEMINUTELOAD}, which is normal. Lower than max value: ${MAXLOAD}."
  exit 0
fi

EMAILSUBJECT="High load: ${FIVEMINUTELOAD} , on ${HOSTNAME}"

echo "========================= Report for ${HOSTNAME} =========================" > ${REPORTFILE} 
echo "System Uptime:" >> ${REPORTFILE}
uptime >> ${REPORTFILE}
separator
# Send the processes list to a file at the moment. Will append this to the report at the end.
# This will make sure that if mysql commands are executed next, they do not taint
# the process list.
ps -eo vsize,pcpu,pmem,pid,ruser,user,comm,stat,time --sort -vsize,-pcpu,-pmem > /tmp/ps.txt

# The following code gets executed only if the DBuser is set, indicating this is a DB server.

if [ "${MYSQLADMINUSER}" != "" ] ; then

  # When the MYSQLADMINUSER is set, but the password is blank, 
  # then it means mysql is configured to allow connection from root without password on the localhost
  if [ "${MYSQLADMINPASSWORD}" == "" ] ; then
    MYSQLPASSWORDOPTION=""
  else
    MYSQLPASSWORDOPTION="-p${MYSQLADMINPASSWORD}"
  fi

  # echo "MYSQLPASSWORDOPTION is set as:${MYSQLPASSWORDOPTION}"

  # Note: mysqladmin version command shows version, as well as brief status.
  echo "mysqladmin version (and status):" >> ${REPORTFILE}
  echo "--------------------------------" >> ${REPORTFILE}
  mysqladmin -u ${MYSQLADMINUSER} ${MYSQLPASSWORDOPTION} version >> ${REPORTFILE}
  separator
  echo "Notes:" >> ${REPORTFILE}
  echo "Threads: The number of active threads (clients). Check the Host column in the processlist output below."  >> ${REPORTFILE}
  echo "Questions:The number of questions (queries) from clients since the server was started." >> ${REPORTFILE}
  echo "Slow queries: The number of queries that have taken more than long_query_time seconds."  >> ${REPORTFILE}
  echo "Opens: The number of tables the server has opened."  >> ${REPORTFILE}
  echo "Open Tables: The number of tables that currently are open." >> ${REPORTFILE}
  echo -n "The value of long_query_time (in seconds) is:" >> ${REPORTFILE}
  mysqladmin -u ${MYSQLADMINUSER} ${MYSQLPASSWORDOPTION}  variables | grep long_query_time | cut -d '|' -f3 >> ${REPORTFILE}
  separator
  echo "mysqladmin processlist:" >> ${REPORTFILE}
  echo "-----------------------" >> ${REPORTFILE}
  mysqladmin -u ${MYSQLADMINUSER} ${MYSQLPASSWORDOPTION} processlist >> ${REPORTFILE}
  separator
fi

echo "5 minute load avg is high: ${FIVEMINUTELOAD}. Generating report of all processes." >> ${REPORTFILE}

## ps -eo "%C %p %u %U %c %x" |sort -r | head -n 10 > /tmp/load_report
## ps -eo vsize,pcpu,pmem,pid,ruser,user,comm,stat,time --sort -vsize,-pcpu,-pmem |  head -n 30 > ${REPORTFILE}

## I need to see all processes, instead of 30 :
echo "" >> ${REPORTFILE}
echo "Executing: ps -eo vsize,pcpu,pmem,pid,ruser,user,comm,stat,time --sort -vsize,-pcpu,-pmem" >> ${REPORTFILE}
echo "" >>  ${REPORTFILE}
### ps -eo vsize,pcpu,pmem,pid,ruser,user,comm,stat,time --sort -vsize,-pcpu,-pmem  >> ${REPORTFILE}
# Append the process list file, we saved in the beginning of the program.
cat /tmp/ps.txt >> ${REPORTFILE}

separator

cat >> ${REPORTFILE} << HELPEOF
PROCESS STATE CODES (From ps man page):
       Here are the different values that the s, stat and state output specifiers
       (header "STAT" or "S") will display to describe the state of a process.
       D    Uninterruptible sleep (usually IO)
       R    Running or runnable (on run queue)
       S    Interruptible sleep (waiting for an event to complete)
       T    Stopped, either by a job control signal or because it is being traced.
       W    paging (not valid since the 2.6.xx kernel)
       X    dead (should never be seen)
       Z    Defunct ("zombie") process, terminated but not reaped by its parent.

       For BSD formats and when the stat keyword is used, additional characters may be
       displayed:
       <    high-priority (not nice to other users)
       N    low-priority (nice to other users)
       L    has pages locked into memory (for real-time and custom IO)
       s    is a session leader
       l    is multi-threaded (using CLONE_THREAD, like NPTL pthreads do)
       +    is in the foreground process group
HELPEOF

separator

echo "Disk space status:" >> ${REPORTFILE}
echo "------------------" >> ${REPORTFILE}
df -hT >> ${REPORTFILE}
separator
echo "Sending Alert email to: ${MAILINGLIST}"  >> ${REPORTFILE}
separator

if [ ${DEBUG} -eq 1 ]; then
  cat ${REPORTFILE}
fi

cat ${REPORTFILE} | /bin/mail -s "${EMAILSUBJECT}" ${MAILINGLIST}
exit 0


