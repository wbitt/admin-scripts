#!/bin/sh
###########################################################################################
# Author:       Muhammad Kamran Azeem (kamran@wbitt.com)
# Created:      2008-04-24
# Modified:     20130803, 20120120, 20080720, 20080628
# Note:         For FTP, make sure that the file ~/.netrc exists in the home directory
#               of the user running this script. The permissions must be 0600.
#               The syntax of .netrc file is:
#               machine ftpserver.somedomain.com
#               login ftpusername
#               password somepassword
#
# For MySQL login without root password, you can also create a .my.cnf file in the user's home directory.
# Normally in the /root/ as most of the time it is root executing cron jobs.
# .my.cnf:
# [mysql]
# user=user
# password=password
#
# Note2:        This script must have permissions not more than 0700, and owned by root . Because
#               this script contains username and password of your mysql admin user.
#
# Note 3:       You will need to frequently delete the old backups both from local disk,
#               as well as from the FTP server.
#
# Note 4:       Setup a cronjob as:-
#               0 3 * * * /backups/dbbackup.sh
#
############################################################################################
#
# User configuration. Start

FTPSERVER=ftpbackup.exampel.com

MYSQLUSER="root"     # MySQL root USERNAME
MYSQLPASS="mysql-password"       # MySQL root PASSWORD
MYSQLHOST="localhost"          # MySQL Hostname

# Backup Dest directory, change this if you have some other location
BACKUPDIR="/data/backups/databases/"

# File to store current backup file
FILE=""

# Store list of databases
DBLIST=""

# DO NOT BACKUP these databases. The list is db names separated by space, enclosed by quotation marks.
IGNOREDBS="information_schema test bb_forums mon phpmyadmin mysql .unison performance_schema"

# This is the time period for which the backups will be retained.
# Backups before this date will be deleted! Make selection carefully.
# The value is number of days before $TODAY.
RETENTIONPERIOD=14



# Backup to FTP location?
SYNCTOFTP="N"
FTPSERVER="ftp.example.com"
FTPUSER="crm@example.com"
FTPPASSWORD="ftp-password"

# You cannot chose FTP client yourself. The script will use lftp for reasons.
# If you don't have it installed, it's time to install it now.

# You can also use ~/.netrc file for FTP, if you dont want to put usernames and password here.
# Just make sure that the ~.netrc file exists in the home directory of the user
# which runs this script. In our case, it is root.

# Perform Sync of vhosts to another localtion on another disk on the same server? 
# e.g. Second Hard drive? Need to specify the TARGETDIR
SYNCTOSECONDDISK="N"
SECONDDISKDIR="/data/backups/databases/"

# Perform Sync to another Server? (Need to specifyu REMOTESERVER, REMOTEUSER and REMOTEDIR)
# Also, make sure that the ssh public key of user root on this server is added to authorized_keys file of the remote user.

SYNCTOREMOTESERVER="N"
REMOTESERVER="www.example.com"
REMOTEUSER="backup"
REMOTEDIR="/backups/db.example.com/databases/"

########## User configuration. End. ###############################################

## TODAY=$(date +%Y%m%d)
TODAY=$(date +%Y%m%d%H%M)
REMOVELASTBACKUPDATE=$(date --date="${RETENTIONPERIOD} days ago" +%Y%m%d)

# Paths to various utlities / programs ...
TAR=$(which tar)
MYSQL=$(which mysql)
MYSQLDUMP=$(which mysqldump)
GZIP=$(which gzip)
BZIP2=$(which bzip2)
RSYNC=$(which rsync)
NICE=$(which nice)
FTPCLIENT=$(which lftp)

# You cannot chose FTP client yourself. The script will use lftp for reasons.
# If you don't have it installed, it's time to install it now.
if [ "${SYNCTOFTP}" == "Y" ] && [ -z "$(which lftp)" ] ; then         
  echo "Please install lftp. Or, switch off SYNCTOFTP directive."
  exit 1
fi


# Main directory where today's backups will be stored
TODAYBACKUPDIR=${TODAY}

# Get hostname
HOSTNAME=$(hostname)
## HOSTNAME="db.example.com"

echo "Here is the configuration, you specified:-"
echo ""
echo "MYSQLUSER=${MYSQLUSER}"
echo "MYSQLPASS=[Edit this script file to see the password]!"
echo "MYSQLHOST=${MYSQLHOST}"
echo "BACKUPDIR=${BACKUPDIR}"
echo "TODAYBACKUPDIR=${TODAYBACKUPDIR}"
echo "RETENTIONPERIOD=${RETENTIONPERIOD}"
echo "REMOVELASTBACKUPDATE=${REMOVELASTBACKUPDATE}"
echo "System HOSTNAME: ${HOSTNAME}"
echo "IGNOREDBS=${IGNOREDBS}"
echo "SYNCTOSECONDDISK=${SYNCTOSECONDDISK}"
echo "SECONDDISKDIR=${SECONDDISKDIR}"
echo "SYNCTOREMOTESERVER=${SYNCTOREMOTESERVER}"
echo "REMOTESERVER=${REMOTESERVER}"
echo "REMOTEUSER=${REMOTEUSER}"
echo "REMOTEDIR=${REMOTEDIR}"

####### Sanaty checking - START ##########
if [ -z "${REMOVELASTBACKUPDATE}" ] || [ "${REMOVELASTBACKUPDATE}" == "/" ] || [ -z "${BACKUPDIR}" ] || [ "${BACKUPDIR}" == "/" ] ; then
  echo "REMOVELASTBACKUPDATE , and BACKUPDIR cannot be empty, nor it can be equal to / ."
  echo "If you do so, you can potentially  WIPE THE DISK CLEAN."
  echo "Fix this, and re-run the program."
  exit 9
fi

if [ "${SYNCTOFTP}" == "Y" ] && [ -z "$(which ${FTPCLIENT})" ] ; then
  echo "Please install lftp."
  exit 9
fi

####### Sanaty checking - END ##########

# Initiating main program


if [ ! -d ${BACKUPDIR}/${TODAYBACKUPDIR} ]; then
  mkdir -p ${BACKUPDIR}/${TODAYBACKUPDIR}
else
  echo ""
  echo "Oops! Old backup found! Perhaps the backup for this date (${TODAY}) has already been performed."
  echo "If this is not what you wanted, you need to manually remove the directory ${BACKUPDIR}/${TODAYBACKUPDIR}"
  echo ""
  exit 1
fi



echo "Starting DB backup/dump...."


# Get all database list first
DBLIST=$($MYSQL -u $MYSQLUSER -h $MYSQLHOST -p$MYSQLPASS -Bse 'show databases')

for DB in ${DBLIST}; do
  skipdb="F"
  if [ "${IGNOREDBS}" != "" ]; then
    for i in ${IGNOREDBS} ; do
      if [ "${DB}" == "${i}" ] ; then
        echo "***** Ignoring DB: ${DB}"
	skipdb="T"
      fi
    done
  fi

  if [ "${skipdb}" == "F" ] ; then
    FILE="${BACKUPDIR}/${TODAYBACKUPDIR}/${DB}.${HOSTNAME}.${TODAY}.dump"
    echo "--------------------------------------------------------------------"
    # do all in one job in pipe,
    # connect to mysql using mysqldump for select mysql database
    # and pipe it out to gz file in backup dir :)
    echo "Processing Database ${DB}....."
    echo "Creating ${FILE}..."
    echo "Executing: ${NICE} -n 19 ${MYSQLDUMP}  --routines --triggers -u ${MYSQLUSER} -h ${MYSQLHOST} -p${MYSQLPASS} ${DB} > ${FILE}"
    ${NICE} -n 19 ${MYSQLDUMP} --routines --triggers  -u ${MYSQLUSER} -h ${MYSQLHOST} -p${MYSQLPASS} ${DB} > ${FILE}
    # We will compress this tar file later.
  fi
done

echo ""
echo ""
echo "Database Dump complete."
echo ""
echo "============================================================================="
echo ""
echo ""
echo ""
echo ""
echo "Starting dump compression to save space on disk. This WILL take some time. Please wait for the process to finish."
echo "Executing: time ${NICE} -n 19 ${BZIP2} -v -9 ${BACKUPDIR}/${TODAYBACKUPDIR}/*.dump"
time ${NICE} -n 19 ${BZIP2} -v -9 ${BACKUPDIR}/${TODAYBACKUPDIR}/*.dump 

echo ""
echo ""
echo "Dump compression complete."
echo ""
echo "============================================================================="
echo ""
echo ""
echo ""
echo ""
echo ""

#if [ "${SYNCTOFTP}" == "Y" ] || [ "${SYNCTOFTP}" == "y" ] ; then
  #echo "Starting FTP upload..."

  #cd $BACKUPDIR/$TODAYBACKUPDIR/

  ## Create a directory on ftp server with today's date.
  #$FTP $FTPSERVER << EOF
  #mkdir $TODAY
  #cd $TODAY
  #mput *.dump.bz2
  #ls -aL
  #EOF

  #cd -

  #echo "FTP Upload complete."

#fi



echo "Time to remove old backups from local system  and remote FTP server."
echo "First remove the directory $BACKUPDIR/$REMOVELASTBACKUPDATE from local system..."
echo "NOTE: Make sure you don't have any typing mistake in the next lines below,"
echo "as IT HAS POTENTIAL TO WIPE OFF YOUR SERVER HARD DISK CLEAN !!!"
echo "YOU HAVE BEEN WARNED."


#cd $BACKUPDIR
if [ -z "${BACKUPDIR}" ] || [ -z "${REMOVELASTBACKUPDATE}" ] ; then
  echo "Cannot remove old backup ${BACKUPDIR}/${REMOVELASTBACKUPDATE}. This seems to be your system root!"
else
  echo "Executing: rm -fr ${BACKUPDIR}/${REMOVELASTBACKUPDATE}*"
  rm -fr ${BACKUPDIR}/${REMOVELASTBACKUPDATE}* && echo "Old backup directory successfully removed."
fi 

#echo "Deleting $REMOVELASTBACKUPDATE from $FTPSERVER..."
#$FTP $FTPSERVER << EOF
#rm -fr  $REMOVELASTBACKUPDATE
#ls -aL
#quit
#EOF

#cd -


if [ "${SYNCTOREMOTESERVER}" == "Y" ] || [ "${SYNCTOREMOTESERVER}" == "y" ]; then

  echo "Starting sync of databases to remote server ${REMOTESERVER}"
  # It is important to use nice (+ve)19 , so our backup process is not a burden
  # on the running system's resources.
  echo "Executing: ${NICE} -n 19 ${RSYNC} -av   ${BACKUPDIR}/${TODAYBACKUPDIR} ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/"
  ${NICE} -n 19 ${RSYNC} -av   ${BACKUPDIR}/${TODAYBACKUPDIR}  ${REMOTEUSER}@${REMOTESERVER}:${REMOTEDIR}/
  echo "Sync of local databases to remote server completed."

fi

exit 0


